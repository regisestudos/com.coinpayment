package com.rfsystems.coinpayment.client;

import org.apache.http.impl.client.HttpClients;
import org.brunocvcunha.coinpayments.CoinPayments;
import org.brunocvcunha.coinpayments.model.AddressResponse;
import org.brunocvcunha.coinpayments.model.BasicInfoResponse;
import org.brunocvcunha.coinpayments.model.CreateTransactionResponse;
import org.brunocvcunha.coinpayments.model.ResponseWrapper;
import org.brunocvcunha.coinpayments.requests.CoinPaymentsCreateTransactionRequest;
import org.brunocvcunha.coinpayments.requests.CoinPaymentsGetCallbackAddressRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class CoinClient {

    private Logger logger = LoggerFactory.getLogger(CoinClient.class);

    public void createTransaction(){
        CoinPayments api = CoinPayments.builder()
                .publicKey("0fd65a2c0a1cda753fadccc23d95a6de6737626088af9715a652ed32a38560ac")
                .privateKey("bb511448383bbc595b533435bDD335d9661C041bfDCa6870387d0dd70205f461")
                .client(HttpClients.createDefault()).build();

        ResponseWrapper<BasicInfoResponse> accountInfo;

        ResponseWrapper<CreateTransactionResponse> txResponse = null;
        try {
            txResponse = api.sendRequest(CoinPaymentsCreateTransactionRequest.builder()
                    .amount(1)
                    .currencyPrice("USD")
                    .currencyTransfer("LTCT")
                    .callbackUrl("http://localhost:8080/swagger-ui/index.html#/transaction-controller/")
                    .buyerEmail("velkanknight@gmail.com")
                    .custom("Order XYZ")
                    .build());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        logger.info(txResponse.getResult().getTransactionId() + " - " + txResponse.getResult().getStatusUrl());
    }
    public void consultTransaction(){
        CoinPayments api = CoinPayments.builder()
                .publicKey("0fd65a2c0a1cda753fadccc23d95a6de6737626088af9715a652ed32a38560ac")
                .privateKey("bb511448383bbc595b533435bDD335d9661C041bfDCa6870387d0dd70205f461")
                .client(HttpClients.createDefault()).build();
        try {
            ResponseWrapper<AddressResponse> addressResponseResponseWrapper =
                    api.sendRequest(CoinPaymentsGetCallbackAddressRequest
                    .builder()
                    .currency("LTCT")
                    .IPNUrl("http://localhost:8080/swagger-ui/index.html#/transaction-controller/")
                    .build()
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//        logger.info(addressResponseResponseWrapper.getResult().getTransactionId() + " - " + addressResponseResponseWrapper.getResult().getStatusUrl());

    }

}
