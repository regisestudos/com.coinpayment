package com.rfsystems.coinpayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoinpaymentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoinpaymentApplication.class, args);
	}

}
