FROM openjdk:17

WORKDIR /app
COPY /target/coinpayment-0.0.1-SNAPSHOT.jar /app/api.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "api.jar"]